# Edi

Modulos relacionados con la facturación electrónica
---------------------------------------------------

Aunque el modulo l10n_co_edi_jorels tal como viene es 100% funcional, 
se recomienda instalar ciertos paquetes de OCA adicionales 
y descomentarlos como dependencias en el archivo __manifest__.py,
para agregar mayor funcionalidad:

Para notificaciones emergentes:
    
    web_notify

Para soporte para nota debito:
    
    account_debitnote

Los cuales pueden ser encontrados en los repositorios de OCA.


Para desarrolladores
--------------------

Si usted es un desarrollador, le recomendamos visitar en su lugar 
los modulos del repositorio principal: [https://gitlab.com/jorels-community](https://gitlab.com/jorels-community)