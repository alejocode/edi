l10n_co_edi_jorels
------------------

Copyright (2019-2020) - Jorels SAS

[info@jorels.com](mailto:info@jorels.com)

[https://www.jorels.com](https://www.jorels.com)

Under LGPL (Lesser General Public License)

Contributing
============

Jorge Sanabria - [js@jorels.com](mailto:js@jorels.com)

Leonardo Martinez - [lotharius96@protonmail.ch](mailto:lotharius96@protonmail.ch)
